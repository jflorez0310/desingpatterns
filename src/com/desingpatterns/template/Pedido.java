package com.desingpatterns.template;

public class Pedido {

    private double importeSinIVA;
    private Pais pais;

    public Pedido(Pais pais, double importeSinIVA){
        this.pais = pais;
        this.importeSinIVA = importeSinIVA;
    }

    public double calculaImporteConIVA(){
        return importeSinIVA + calcularIVA();
    }

    public double calcularIVA(){
        return importeSinIVA * this.pais.calculaIVA();
    }
}
