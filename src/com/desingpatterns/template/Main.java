package com.desingpatterns.template;

public class Main {

    public static void main (String[] args) {
        Pedido pedidoFrancia = new Pedido(new Francia(), 2523.00);
        Pedido pedidoColombia = new Pedido(new Colombia(), 4500.00);
        System.out.println("Francia: " + pedidoFrancia.calculaImporteConIVA());
        System.out.println("Colombia: " + pedidoColombia.calculaImporteConIVA());
    }
}
