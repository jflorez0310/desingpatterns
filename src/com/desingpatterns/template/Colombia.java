package com.desingpatterns.template;

public class Colombia extends Pais{

    @Override
    public double calculaIVA() {
        return 19.0/100;
    }
}
