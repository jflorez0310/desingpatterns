package com.desingpatterns.template;

public class Francia extends Pais{

    @Override
    public double calculaIVA() {
        return 14.0/100;
    }
}
