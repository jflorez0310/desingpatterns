package com.designpetterns.abstractfactory;

public class ScooterGasolina extends Scooter{

    public ScooterGasolina(String modelo, String color, int potencia){
        super(modelo, color, potencia);
    }

    @Override
    public void mostrarCaracteristicas() {
        StringBuilder stCaracteristicas = new StringBuilder();
        stCaracteristicas.append("Scooter gasolina de modelo: ")
                .append(modelo)
                .append(" de color: ")
                .append(color)
                .append(" de potencia ")
                .append(potencia);
        System.out.println(stCaracteristicas);
    }


}
