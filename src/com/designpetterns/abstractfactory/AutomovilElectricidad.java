package com.designpetterns.abstractfactory;

public class AutomovilElectricidad extends Automovil {

    public AutomovilElectricidad(String modelo, String color, int potencia, double espacio){
        super(modelo, color, potencia, espacio);
    }
    @Override
    public void mostrarCaracteristicas() {
        StringBuilder stCaracteristicas = new StringBuilder();
        stCaracteristicas.append("Automovil electrico de modelo: ")
                         .append(modelo)
                         .append(" de color: ")
                         .append(color)
                         .append(" de potencia ")
                         .append(potencia)
                         .append(" de espacio: ")
                         .append(espacio);
        System.out.println(stCaracteristicas);
    }
}
