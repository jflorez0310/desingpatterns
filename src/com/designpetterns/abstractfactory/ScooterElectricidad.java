package com.designpetterns.abstractfactory;

public class ScooterElectricidad extends Scooter{

    public ScooterElectricidad(String modelo, String color, int potencia){
        super(modelo, color, potencia);
    }

    @Override
    public void mostrarCaracteristicas() {
        StringBuilder stCaracteristicas = new StringBuilder();
        stCaracteristicas.append("Scooter electricidad de modelo: ")
                .append(modelo)
                .append(" de color: ")
                .append(color)
                .append(" de potencia ")
                .append(potencia);
        System.out.println(stCaracteristicas);
    }
}
